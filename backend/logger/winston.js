const winston = require('winston');
const expressWinston = require('express-winston');

/* If Prod Env: Would set up a less verbose prod configuration for winston to log to server,
as well as integrate winston as pref logger throughout, not just on requests */
const requestLogger = expressWinston.logger({
  transports: [
    new winston.transports.Console(),
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json(),
  ),
  meta: true,
  msg: 'HTTP {{req.method}} {{req.url}}',
  expressFormat: true,
  colorize: false,
});

module.exports = requestLogger;
