/* eslint-disable no-unused-vars */
const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const requestLogger = require('./logger/winston');
const collectionRouter = require('./routes/collectionRoute');

// If Prod Env: Would set up front/backend using proxy or API Gateway, bypassing need for CORS
const corsOptions = {
  origin: 'http://localhost:4200',
  methods: 'GET',
  preflightContinue: true,
  optionsSuccessStatus: 204,
  credentials: true,
};

const app = express();

// If Prod Env: Would import Helmet to prevent various attack vectors (i.e XSS, MIME Sniffing.. )
app.use(cors(corsOptions));
app.use(requestLogger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// If Prod Env: Would consider API versioning
app.use('/api/swc/getMyStarWarsCollection', collectionRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
});

module.exports = app;
