/* eslint-disable no-unused-vars */
const express = require('express');
const service = require('../services/mockService');

const router = express.Router();

router.get('/', (req, res) => {
  try {
    const data = service.mockGetData(req.cookies.userName);
    res.json(data);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }
});

module.exports = router;
